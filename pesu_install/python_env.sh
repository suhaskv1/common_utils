#!/bin/bash
set -e
# Run this bash file using super user privilege

# Constants
# !!! Add '/' at the end for all the path constants

USER=$USER
PYTHON_VER='3.7'
WORK_DIR='/home/'$USER'/chromite_bb/'
CONDA_INSTALLATION_DIR=${WORK_DIR}'conda/'

welcome() {
  echo Hello $USER! Welcome to Bug Bounty Program from Incore Semiconductors.
  echo This installation script will help you to get started with setting up conda env
  sudo apt-get update
}

conda_env() {
  conda config --set auto_activate_base false
  # Creating new Python 3.7 conda environment named "chromite_bug_bounty"
  conda create --name "chromite_bug_bounty" python=$PYTHON_VER --yes
  # conda env remove -n "chromite_bug_bounty"
  conda activate chromite_bug_bounty
  pip install -U river_core
  pip install aapg
}

chromite() {
  cd ${WORK_DIR}
  conda activate chromite_bug_bounty
  git clone "https://gitlab.com/incoresemi/core-generators/chromite.git"
  cd chromite
  pip3 install -U -r requirements.txt
  python -m configure.main -ispec sample_config/default.yaml
  make generate_verilog
  make link_verilator
  conda deactivate
}

close() {
  echo 'Finshed setting up Conda Environment as well as building Chromite'
}

welcome
conda_env
chromite
close

===================
Chromite Bug Bounty
===================

Requirements
===================

.. note:: The following have been tested on Ubuntu 20.04.

We suggest you to have a separate directory for the Chromite Bug Bounty related tools. It shall be named ``chromite_bb`` and stored in ``~/chromite_bb``

.. code::

    mkdir ~/chromite_bb

Install the dependencies needed to install the tools.

.. code::

    sudo apt-get update
    sudo apt-get -y install -y autoconf automake autotools-dev bc bison build-essential ccache curl device-tree-compiler flex g++ gawk ghc git gperf gtkwave itcl3-dev itk3-dev iverilog libcanberra-gtk-module libcanberra-gtk3-module libexpat-dev libfontconfig1-dev libghc-old-time-dev libghc-regex-compat-dev libghc-split-dev libghc-syb-dev libgmp-dev libgoogle-perftools-dev libmpc-dev libmpfr-dev libtool libx11-dev libxft-dev make numactl patchutils perl perl-doc pkg-config python3 tcl-dev texinfo tk-dev zlib1g-dev


Installing Bluespec Compiler
============================

`Bluespec <https://github.com/B-Lang-org/bsc>`_ is a High Level Hardware description language. For more information on installation refer, `compiling from Source <https://github.com/B-Lang-org/bsc/blob/main/INSTALL.md>`_. 

- Clone the repository and intall bsc:

.. code::

    mkdir ~/chromite_bb/bluespec/
    cd ~/
    git clone --recursive https://github.com/B-Lang-org/bsc
    cd bsc
    git checkout 2021.07
    make PREFIX=~/chromite_bb/bluespec/ install-src

- If you would like to build the docs (Reference and User guide for BSV) then texlive also needs to be installed (OPTIONAL):

.. code::

    sudo apt-get install texlive-latex-base texlive-latex-recommended texlive-latex-extra texlive-font-utils texlive-fonts-extra
    make PREFIX=~/chromite_bb/bluespec/ install-doc

- Update your $PATH. Bash users can do the following:

.. code::

    echo 'export PATH=$PATH:~/chromite_bb/mybsc//bin' >> ~/.bashrc 

- Check if ``bsc`` is successfully installed by running the following code from a new terminal:

.. code::
    
    bsc --help

.. note:: the ``-j`` or ``--jobs`` option tells ``make`` to execute many recipes simultaneously. If the ‘-j’ option is followed by an integer, this is the number of recipes to execute at once; this is called the number of job slots. If there is nothing looking like an integer after the ‘-j’ option, there is no limit on the number of job slots. The default number of job slots is one, which means serial execution (one thing at a time). The user can choose to use the ``-j`` option with ``make`` the make commands that follow.

Verilator Verilog Simulator
===========================

`Verilator <https://www.veripool.org/verilator/>`_ is a fast open-source Verilog/SystemVerilog simulator. For a detailed installation guide, refer this `link <https://verilator.org/guide/latest/install.html>`_. **DO NOT INSTALL FROM PACKAGE MANAGER!**. We need a specific version of Verilator(4.106), hence better to go with git installation. 
 
**PLEASE CHANGE '<user>' in the commands to your username.**

.. code::
    
    ## Following is for Ubuntu systems only (ignore is raises error)
    sudo apt-get -y install libfl2 libfl-dev zlib1g zlib1g-dev

    mkdir ~/chromite_bb/my_verilator/
    cd ~/
    git clone https://github.com/verilator/verilator 
    unset VERILATOR_ROOT  # For bash shell 
    cd verilator 
    git checkout v4.106 
    autoconf 
    ./configure --prefix=/home/<user>/chromite_bb/myverilator/
    make 
    make install 
    echo 'export PATH=$PATH:~/chromite_bb/myverilator//bin' >> ~/.bashrc


GNU Toolchain Installation
==========================

.. note::
    
    If you are using RHEL/CentOS/ArchLinux or OS X please refer to the installation guide `here <https://github.com/riscv-software-src/riscv-gnu-toolchain>`_ for downloading pre-requisites.

- Clone and install toolchain:

.. code::

    mkdir ~/chromite_bb/myriscv/
    cd ~/
    git clone https://github.com/riscv-collab/riscv-gnu-toolchain.git
    cd riscv-gnu-toolchain
    git checkout 1a36b5dc44d71ab6a583db5f4f0062c2a4ad963b
    ./configure --prefix=/home/<user>/chromite_bb/myriscv/ --with-arch=rv64imac --with-abi=lp64 --with-cmodel=medany
    make
    echo 'export PATH=$PATH:~/chromite_bb/myriscv/bin' >> ~/.bashrc

Spike - Instruction Set Simulator
=================================

While this is useful for porting applications, we also need the elf2hex utility embedded in this tool for simulations

.. code::


    mkdir ~/chromite_bb/myspike/
    cd ~/
    git clone https://github.com/riscv-software-src/riscv-isa-sim.git
    cd riscv-isa-sim
    git checkout 1afbe648b8eb36bd9caf641d8d5365ffb3069557
    mkdir build
    cd build
    ../configure --prefix=/home/<user>/chromite_bb/myspike/bin --enable-commitlog
    make
    make install
    echo 'export PATH=$PATH:~/chromite_bb/myspike//bin' >> ~/.bashrc

Miniconda 
=========

- `Miniconda <https://docs.conda.io/en/latest/miniconda.html>`_ is a lightweight installer for conda (a python package management system). It will help us in creating, managing multiple python environments. Using python environments, we can isolate issues, dependency problems from affecting other environments or the system.

.. code::
    
    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/chromite_bb/miniconda.sh 
    # yes to accept licence, enter to default location, yes to conda init 
    bash ~/chromite_bb/miniconda.sh 
    ~/miniconda3/bin/conda init bash 
    echo 'export PATH=~/chromite_bb/miniconda3/bin:$PATH' >>~/.bashrc 

- Make sure to open a new terminal for changes in bashrc to be reflected and do the following:

.. code::
    
    conda config --set auto_activate_base false 


Creating Conda environment
==========================
- We will be creating an environment specically for the Bug Bounty program. So we are naming the environment “chromite_bb”. `This <https://www.youtube.com/watch?v=1VVCd0eSkYc>`_ video explains more about conda environments. 

.. code::
    conda create --name "chromite_bb" python=3.7 --yes 
    conda activate chromite_bb 

- Now install the python modules/dependencies needed for the bounty program:

.. code::
    conda activate chromite_bb
    pip3 install -U river_core
    pip3 install aapg
